import random
import copy
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as Fnn
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
from torch.autograd import Variable
import torch.optim
from Agent.PPO.PPO_utils import gae
# https://github.com/lnpalmer/PPO/blob/master/ppo.py reformuler
gamma = 0.99
batch_size = 32
max_grad_norm = .5
worker_steps = 128
nb_workers = 8
lambd = .95  # GAE lambda parameter
lr = 1e-5
opt_epoch = 3
# clip =
is_cuda_available = torch.cuda.is_available()


class PPO_runner :
    def __init__(self, venv, test_env, model, clip_func, lr_func, value_coef = 1, entropy_coef = 0.01,  max_grad_norm=.5) :
        # Venv the vectorized environment to use
        self.optimizer = torch.optim.Adam(model.parameters(), lr = lr)
        self.venv = venv
        self.test_env = test_env
        self.model = model
        self.model_old = copy.deepcopy(model)
        if is_cuda_available:
            self.model = self.model.cuda()
            self.model_old = self.model_old.cuda()
        self.ep_reward = np.zeros(nb_workers)
        self.reward_history = []
        self.step_history = []
        self.objective = PPOObjective()
        self.last_ob = self.venv.reset()
        self.taken_steps = 0
        self.test_repeat_max = 100
        self.clip_func = clip_func
        self.max_grad_norm = max_grad_norm
        self.value_coef = value_coef
        self.entropy_coef = entropy_coef
        self.lr_func = lr_func

    def run(self, total_steps) :
        self.mean_rew_history = []
        self.rew_history = []
        while self.taken_steps < total_steps :


            progress = self.taken_steps / total_steps
            print("Progress : ",progress * 100, "%")

            states, rewards, dones, actions, steps = self.interact()
            self.rew_history.append(torch.sum(rewards).cpu())

            self.mean_rew_history.append(np.mean(self.rew_history))
            observation_shape = states.size()[2:] # Dans states sont stockes aussi Steps et NbWorker
            ep_reward = self.test()
            self.reward_history.append(ep_reward)
            self.step_history.append(self.taken_steps)
            states_pred = states.view(((worker_steps + 1) * nb_workers,) + observation_shape) # NANI, le step dans le state Oo  ?
            states_pred = Variable(states_pred)

            _, values = self.model(states_pred)
            values = values.view(worker_steps + 1, nb_workers, 1)
            advantages, returns = gae(rewards, dones, values, gamma, lambd)
            self.model_old.load_state_dict(self.model.state_dict())
            for epoch in range(opt_epoch):
                self.model.zero_grad()
                batch_states = Variable(states[:worker_steps].view((steps,) + observation_shape))
                batch_rewards = Variable(rewards.view(steps, 1))
                batch_dones = Variable(dones.view(steps, 1))
                batch_actions = Variable(actions.view(steps, 1))
                batch_advantages = Variable(advantages.view(steps, 1))
                batch_returns = Variable(returns.view(steps, 1))

                batch_indices = np.arange(steps)
                np.random.shuffle(batch_indices)
                for start in range(0, steps, batch_size):
                    minibatch_indices = batch_indices[start:start + batch_size]
                    if is_cuda_available:
                        minibatch_indices = torch.tensor(minibatch_indices).cuda()
                        #minibatch_indices = torch.from_numpy(minibatch_indices).cuda()
                    minibatch_states, minibatch_rewards, minibatch_dones,\
                    minibatch_actions, minibatch_advantages, minibatch_returns =\
                        [arr[minibatch_indices] for arr in [batch_states, batch_rewards, batch_dones,\
                                                            batch_actions, batch_advantages, batch_returns]]
                    if is_cuda_available:
                        minibatch_states = torch.tensor(minibatch_states).cuda()
                    minibatch_probability, minibatch_value = self.model(minibatch_states)
                    minibatch_probability_old, minibatch_value_old = self.model_old(minibatch_states)
                    minibatch_probability_old = minibatch_probability_old.detach()
                    minibatch_value_old = minibatch_value_old.detach()

                    losses = self.objective(self.clip_func(progress), \
                                            minibatch_probability, minibatch_value, minibatch_probability_old, \
                                            minibatch_value_old, minibatch_actions, minibatch_advantages, minibatch_returns)
                    policy_loss, value_loss, entropy_loss = losses
                    loss = policy_loss + value_loss * self.value_coef + entropy_loss * self.entropy_coef

                    for param_group in self.optimizer.param_groups :
                        param_group['lr'] = self.lr_func(progress)
                    self.optimizer.zero_grad()
                    loss.backward()
                    torch.nn.utils.clip_grad_norm(self.model.parameters(), self.max_grad_norm)
                    self.optimizer.step()
            self.taken_steps += steps









    def interact(self) :
        observations = torch.zeros(worker_steps + 1, nb_workers, 4, 84, 84)
        rewards = torch.zeros(worker_steps, nb_workers, 1)
        dones = torch.zeros(worker_steps, nb_workers, 1)
        actions = torch.zeros(worker_steps, nb_workers, 1).long()
        if is_cuda_available :
            observations = observations.cuda()
            rewards = rewards.cuda()
            dones = dones.cuda()
            actions = actions.cuda()

        for time in range(worker_steps) :  # Collect trajectories
            ob = torch.from_numpy(self.last_ob.transpose((0, 3, 1, 2))).float()  # Nani, remet le 4 en 2e position au lieu de 4e
            ob = Variable(ob / 255.) # Normalise
            if is_cuda_available :
                ob = ob.cuda()
            observations[time] = ob.data # [NbWorker, 4, 84, 84]
            probability, value = self.model(ob)
            u = torch.rand(probability.size())
            if is_cuda_available :
                u = u.cuda()
            pos_max, action = torch.max(probability.data - (-u.log()).log(), 1) # bizarre, a regarde le sens
            action = action.unsqueeze(1)
            actions[time] = action
            self.last_ob, reward, done, _ = self.venv.step(action.cpu().numpy())
            reward = torch.from_numpy(reward).unsqueeze(1)
            rewards[time] = torch.clamp(reward, min = -1., max = 1.)
            dones[time] = torch.from_numpy((1 - done)).unsqueeze(1)
        ob = torch.from_numpy(self.last_ob.transpose((0, 3, 1, 2))).float()
        ob = Variable(ob / 255.)
        if is_cuda_available :
            ob = ob.cuda()
        observations[time] = ob

        steps = nb_workers * worker_steps
        return observations, rewards, dones, actions, steps

    def test(self) :
        observation = self.test_env.reset()
        done = False
        ep_reward = 0
        last_action = np.array([-1])
        action_repeat = 0
        self.model.eval()
        while not done :
            ob = np.array(observation)
            ob = torch.from_numpy(ob.transpose((2, 0, 1))).float().unsqueeze(0)
            ob = Variable(ob / 255)
            if is_cuda_available :
                ob = ob.cuda()
            proba, value = self.model(ob)
            _, action = torch.max(proba, dim = 1)  # Verifier qu'il faut pas choice
            if action.data[0] == last_action.data[0] :
                action_repeat += 1
                if action_repeat == self.test_repeat_max :
                    return ep_reward
            else :
                action_repeat = 0
            last_action = action
            obs, reward, done, _ = self.test_env.step(action.data.cpu().numpy())
            ep_reward += reward
        self.model.train()
        return ep_reward


class PPOObjective(nn.Module) :
    def forward(self, clip, pi, v, pi_old, v_old, action, advantage, returns) :
        prob = Fnn.softmax(pi)  # Feedforward neural network
        log_prob = Fnn.log_softmax(pi)
        action_prob = prob.gather(1, action)

        prob_old = Fnn.softmax(pi_old)
        action_prob_old = prob_old.gather(1, action)

        ratio = action_prob / (action_prob_old + 1e-10)

        advantage = (advantage - advantage.mean()) / (advantage.std() + 1e-5)

        surr1 = ratio * advantage
        surr2 = torch.clamp(ratio, min = 1. - clip, max = 1. + clip) * advantage

        policy_loss = -torch.min(surr1, surr2).mean()
        value_loss = (.5 * (v - returns) ** 2.).mean()
        entropy_loss = (prob * log_prob).sum(1).mean()

        return policy_loss, value_loss, entropy_loss
