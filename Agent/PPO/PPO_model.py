
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable


class PPO_CNN(nn.Module):
    def __init__(self, n_actions):
        # Actor critic like
        super().__init__()
        self.conv = nn.Sequential(nn.Conv2d(4, 32, 8, stride = 4),
                                  nn.ReLU(inplace=True),
                                  nn.Conv2d(32, 64, 4, stride = 2),
                                  nn.ReLU(inplace = True),
                                  nn.Conv2d(64, 64, 3, stride = 1),
                                  nn.ReLU(inplace = True))
        self.fc = nn.Sequential(nn.Linear(64 * 7 * 7, 512),
                                nn.ReLU(inplace = True))
        self.probability = nn.Linear(512, n_actions)
        self.value = nn.Linear(512, 1)
        self.n_actions = n_actions

    def forward(self, input):
        # Input : [N x 4 x 84 x 84]
        # Output : Return action probabolity [N X Num Actions] and Value prediction [N x 1]
        batch_size = input.size()[0]
        if torch.cuda.is_available():
            input = input.cuda()
        conv_out = self.conv(input).view(batch_size, 64 * 7 * 7)
        fc_out = self.fc(conv_out)
        probability_out = self.probability(fc_out)
        value_out = self.value(fc_out)

        return probability_out, value_out

