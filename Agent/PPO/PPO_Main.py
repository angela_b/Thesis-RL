import torch
import torch.optim as optim
from baselines.common.atari_wrappers import FrameStack
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from baselines.common.vec_env.vec_frame_stack import VecFrameStack

from Agent.PPO.PPO_envs import make_env
from Agent.PPO.PPO_model import PPO_CNN
from Agent.PPO.PPO_runner import PPO_runner

opt_epochs = 3
total_steps = int(10e6)
worker_steps = 128
sequence_steps = 32
minibatch_Steps = 256
lr = 2.5e-4
l_func = 'linear'
clipping = .1
clip_fc = 'linear'
gamma = .99
lambd = .95
value_coef = 1
entropy_coef = .01
max_grad_norm = .5

render_interval = 4




cuda = torch.cuda.is_available()

env_fns = []
for rank in range(8) :
    env_fns.append(lambda : make_env("BreakoutNoFrameskip-v0", rank, 42 + rank))

venv = SubprocVecEnv(env_fns, render_interval)
venv = VecFrameStack(venv, 4)

test_env = make_env("BreakoutNoFrameskip-v0", 0, 42)
test_env = FrameStack(test_env, 4)

policy = {'cnn' : PPO_CNN}["cnn"](venv.action_space.n)
print("Policy : ", policy)
if torch.cuda.is_available():
    policy = policy.cuda()




if l_func == 'linear' :
    lr_func = lambda a : lr * (1. - a)
elif l_func == 'constant' :
    lr_func = lambda a : lr

if clip_fc == 'linear' :
    clip_func = lambda a : clipping * (1. - a)
elif clip_fc == 'constant' :
    clip_func = lambda a : clipping
try:
    algorithm = PPO_runner(venv, test_env,policy,
                    lr_func = lr_func, clip_func = clip_func,
                    value_coef = value_coef, entropy_coef = entropy_coef,
                    max_grad_norm = max_grad_norm)
    algorithm.run(total_steps)
except KeyboardInterrupt:
    print("Cleaning")
finally:
    venv.close()