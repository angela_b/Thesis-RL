import random
import numpy as np
import torch


def gae(rewards, masks, values, gamma, lambd):

    T, N, _ = rewards.size()

    cuda = rewards.is_cuda
    advantages = torch.zeros(T, N, 1)
    advantage_t = torch.zeros(N, 1)
    if cuda :
        advantages = advantages.cuda()
        advantage_t = advantage_t.cuda()

    for t in reversed(range(T)):
        delta = rewards[t] + values[t + 1].data * gamma * masks[t] - values[t].data
        advantage_t = delta + advantage_t * gamma * lambd * masks[t]
        advantages[t] = advantage_t

    returns = values[:T].data + advantages

    return advantages, returns
